<?php
namespace App\Http\Services;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
class FatoorahServices {

    private $base_url;
    private $header;
    private $request_client;

    /**
     * FatoorahService coustructor.
     * @param Client $request_client
     */

    public function __construct(Client $request_client){
        $this->request_client=$request_client;

        $this->base_url=env(key:'fatoorah_base_url');
        $this->headers=[
            'Content-type'=>'application/json',
            'authorization'=>'Bearer'.env(key: 'fatoorah_token')
        ];

    }

/**
 * @param $uri
 * @param $method
 * @param array $body
 * @return false/mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */

private function buildRequest($uri,$method,$body=[])
{
$request= new Request($method, $this->base_url. $uri, $this->header);
if(!$body)
return false;
$response= $this->request_client->send($request,[
    'json'=>$body

]);
if ($response->getStatusCode() !=200){
    return false;
}
$response=json_decode($response->getBody(), true);
return $response;
}

}
