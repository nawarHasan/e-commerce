<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            [
                'name'=>'Lion ',
                "price"=>"1000",
                "description"=>"Strong and fast",
                "category"=>"Jaguar",
                "gallery"=>"/imgs/images (1).jpg"
                // "gallery"=>"https://assetscdn1.paytm.com/images/catalog/product/M/MO/MOBOPPO-A52-6-GFUTU6297453D3D253C/1592019058170_0..png"
            ],
            [
                'name'=>'Mer-Car',
                "price"=>"2000",
                "description"=>"A smart with alot of features",
                "category"=>"Mercedes-Bens",
                "gallery"=>"/imgs/images 4.jpg"
            ],
            [
                'name'=>'Tigger',
                "price"=>"500",
                "description"=>"Pretty car and smart",
                "category"=>"Formula",
                "gallery"=>"/imgs/images (3).jpg"
                // "gallery"=>"https://4.imimg.com/data4/PM/KH/MY-34794816/lcd-500x500.png"
            ],
            [
                'name'=>'Sonic',
                "price"=>"1200",
                "description"=>"Very Fast and cool ",
                "category"=>"Audi",
                "gallery"=>"/imgs/images (2).jpg"
                // "gallery"=>"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFx-2-wTOcfr5at01ojZWduXEm5cZ-sRYPJA&usqp=CAU"
             ]
        ]);
    }
}
