// const MMLToSVG = require("mml-to-svg");
import {MMLToSVG} from "mml-to-svg";
// ../node_modules/mml-to-svg/index.d.ts/mml-to-svg";

const myMMLEquation = `<math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
        <mi>x</mi> <mo>=</mo>
        <mrow>
          <mfrac>
            <mrow>
              <mo>&#x2212;</mo>
              <mi>b</mi>
              <mo>&#x00B1;</mo>
              <msqrt>
                <msup><mi>b</mi><mn>2</mn></msup>
                <mo>&#x2212;</mo>
                <mn>4</mn><mi>a</mi><mi>c</mi>
              </msqrt>
            </mrow>
            <mrow> <mn>2</mn><mi>a</mi> </mrow>
          </mfrac>
        </mrow>
        <mtext>.</mtext>
      </math>`;

const options = {
    width: 1280,
    ex: 8,
    em: 16
};

const SVGEquation = MMLToSVG(myMMLEquation, options)
console.log(123)
